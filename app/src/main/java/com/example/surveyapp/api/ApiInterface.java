package com.example.surveyapp.api;

import com.example.surveyapp.models.Answer;
import com.example.surveyapp.models.Question;
import com.example.surveyapp.models.Survey;
import com.example.surveyapp.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @GET("all_surveys.php")
    Call<List<Survey>> getAllSurveys();

    @FormUrlEncoded
    @POST("my_surveys.php")
    Call<List<Survey>> getMySurveys(
            @Field("user_id") int userId
    );

    @GET("users.php")
    Call<List<User>> getUsers();

    @FormUrlEncoded
    @POST("questions.php")
    Call<List<Question>> getQuestions(
      @Field("survey_id") int surveyId
    );

    @FormUrlEncoded
    @POST("answers.php")
    Call<List<Answer>> getAnswers(
            @Field("question_id") int questionId
    );

    @FormUrlEncoded
    @POST("new_answer.php")
    Call<Answer> updateAnswerSelections(
            @Field("answer_id") int answerId,
            @Field("selections") int selections
    );

    @FormUrlEncoded
    @POST("register.php")
    Call<User> insertUser(
            @Field("login") String login,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("insert_survey.php")
    Call<List<Survey>> insertSurvey(
            @Field("user_id") int userId,
            @Field("name") String name,
            @Field("description") String description,
            @Field("start_date") String startDate,
            @Field("end_date") String endDate
    );

    @FormUrlEncoded
    @POST("update_survey.php")
    Call<List<Survey>> updateSurvey(
            @Field("survey_id") int surveyId,
            @Field("name") String name,
            @Field("description") String description,
            @Field("start_date") String startDate,
            @Field("end_date") String endDate
    );

    @FormUrlEncoded
    @POST("insert_question.php")
    Call<List<Question>> insertQuestion(
            @Field("survey_id") int surveyId,
            @Field("content") String content
    );

    @FormUrlEncoded
    @POST("insert_answer.php")
    Call<Answer> insertAnswer(
            @Field("question_id") int questionId,
            @Field("content") String content
    );
}
