package com.example.surveyapp.room;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.surveyapp.models.User;

import java.util.List;

public class UserRepository {
    private UserDao userDao;
    private LiveData<List<User>> users;

    UserRepository(Application application) {
        UserDatabase database = UserDatabase.getDatabase(application);
        userDao = database.userDao();
        users = userDao.findAll();
    }

    void insert(User user) {
        UserDatabase.dataBaseWriteExecutor.execute(() -> {
            userDao.insert(user);
        });
    }

    void delete(User user) {
        UserDatabase.dataBaseWriteExecutor.execute(() -> {
            userDao.delete(user);
        });
    }

    LiveData<List<User>> findAll() {
        return users;
    }
}
