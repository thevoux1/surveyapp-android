package com.example.surveyapp.room;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.surveyapp.models.User;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class UserDatabase extends RoomDatabase {
    public abstract UserDao userDao();

    private static volatile UserDatabase INSTANCE;
    public static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService dataBaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static UserDatabase getDatabase(final Context context) {
        if(INSTANCE == null) {
            synchronized (UserDatabase.class) {
                if(INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            UserDatabase.class, "user_db")
                            //.addCallback(sRoomDataBaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /*private static RoomDatabase.Callback sRoomDataBaseCallback = new RoomDatabase.Callback(){
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            dataBaseWriteExecutor.execute(() -> {
                BookDao dao = INSTANCE.bookDao();
                dao.deleteAll();

                dao.insert(new Book("The Lord of the Rings", "J.R.R. Tolkien"));
                dao.insert(new Book("The Chronicles of Narnia", "C.S. Lewis"));
                dao.insert(new Book("Ulysses Moore", "Pierdomenico Baccalario"));
                dao.insert(new Book("Harry Potter and the Sorcerer's Stone", "J.K. Rowling"));
                dao.insert(new Book("And Then There Were None", "Agatha Christie"));
                dao.insert(new Book("A Tale of Two Cities", "Charles Dickens"));
                dao.insert(new Book("Don Quixote", "Miguel de Cervantes"));
            });
        }
    };*/
}

