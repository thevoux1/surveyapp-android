package com.example.surveyapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.example.surveyapp.R;
import com.example.surveyapp.api.ApiClient;
import com.example.surveyapp.api.ApiInterface;
import com.example.surveyapp.models.User;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private EditText loginEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText repeatPasswordEditText;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        loginEditText = findViewById(R.id.register_login_edit_text);
        emailEditText = findViewById(R.id.register_email_edit_text);
        passwordEditText = findViewById(R.id.register_password_edit_text);
        repeatPasswordEditText = findViewById(R.id.repeat_password_edit_text);
        registerButton = findViewById(R.id.register_button);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ukrycie klawiatury
                loginEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                emailEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                passwordEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                repeatPasswordEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);

                if(loginEditText.getText().toString().trim().equals("")) {
                    displaySnackbar(getResources().getString(R.string.empty_login));
                    return;
                }

                if(emailEditText.getText().toString().trim().equals("")) {
                    displaySnackbar(getResources().getString(R.string.empty_email));
                    return;
                }

                if(passwordEditText.getText().toString().trim().equals("") || repeatPasswordEditText.getText().toString().trim().equals("")) {
                    displaySnackbar(getResources().getString(R.string.empty_password));
                    return;
                }

                if(passwordEditText.getText().toString().length() < 8) {
                    displaySnackbar(getResources().getString(R.string.too_short_password));
                    return;
                }

                if(!passwordEditText.getText().toString().equals(repeatPasswordEditText.getText().toString())) {
                    displaySnackbar(getResources().getString(R.string.incompatible_passwords));
                    return;
                }

                getUsers();
            }
        });
    }

    private void displaySnackbar(String message) {
        Snackbar.make(findViewById(R.id.log_in_view), message,
                Snackbar.LENGTH_LONG).show();
    }

    private void getUsers() {
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<List<User>> call = apiInterface.getUsers();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                checkUniqueness(response.body());
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                displaySnackbar(getResources().getString(R.string.something_went_wrong));
            }
        });
    }

    private void checkUniqueness(List<User> users) {
        String login = loginEditText.getText().toString().trim();
        String email = emailEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();

        User foundUser = null;
        for(User user : users) {
            if(user.getLogin().equals(login))
            {
                foundUser = user;
                break;
            }
        }

        if(foundUser != null) {
            displaySnackbar(getResources().getString(R.string.login_exists));
            return;
        }

        foundUser = null;
        for(User user : users) {
            if(user.getEmail().equals(email))
            {
                foundUser = user;
                break;
            }
        }

        if(foundUser != null) {
            displaySnackbar(getResources().getString(R.string.email_exists));
            return;
        }

        registerUser(login, email, password);
    }

    private void registerUser(String login, String email, String password) {
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<User> call = apiInterface.insertUser(login, email, password);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                displaySnackbar(getResources().getString(R.string.something_went_wrong));
            }
        });
    }
}