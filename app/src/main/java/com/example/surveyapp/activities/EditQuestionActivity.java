package com.example.surveyapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.surveyapp.R;
import com.example.surveyapp.models.Answer;
import com.google.android.material.snackbar.Snackbar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EditQuestionActivity extends AppCompatActivity {
    public static final String EXTRA_ANSWER_LIST = "AnswerList";
    public static final String EXTRA_QUESTION_CONTENT = "QuestionContent";

    private List<Answer> answers;
    private EditText questionEditText;
    private RadioGroup answersRadioGroup;
    private Button addAnswerButton;
    private EditText answerEditText;
    private Button addQuestionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_question);

        questionEditText = findViewById(R.id.question_edit_text);
        answersRadioGroup = findViewById(R.id.edit_answers_radiogroup);
        answerEditText = findViewById(R.id.answer_edit_text);
        addAnswerButton = findViewById(R.id.add_answer_button);
        addQuestionButton = findViewById(R.id.add_edit_question_button);

        if(getIntent().hasExtra(EXTRA_ANSWER_LIST)) {
            questionEditText.setText(getIntent().getStringExtra(EXTRA_QUESTION_CONTENT));
            answers = (List<Answer>) getIntent().getSerializableExtra(EXTRA_ANSWER_LIST);

            for(Answer answer : answers) {
                prepareAnswerRadioGroup(answers.indexOf(answer), answer.getContent());
            }

            setTitle(R.string.edit_question);
        } else {
            answers = new ArrayList<>();
        }

        addAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(answerEditText.getText().toString().trim().equals(""))
                {
                    questionEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                    answerEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);

                    displaySnackbar(getResources().getString(R.string.empty_answer));
                    return;
                }

                prepareAnswerRadioGroup(answers.size(), answerEditText.getText().toString());
                answers.add(new Answer(answerEditText.getText().toString()));
                answerEditText.setText("");
            }
        });

        addQuestionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                answerEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                String content = questionEditText.getText().toString().trim();

                if(content.trim().equals("")) {
                    displaySnackbar(getResources().getString(R.string.empty_question));
                    return;
                }

                if(answers.size() < 2) {
                    displaySnackbar(getResources().getString(R.string.too_few_questions));
                    return;
                }

                Intent respondIntent = new Intent();
                respondIntent.putExtra(EXTRA_ANSWER_LIST, (Serializable) answers);
                respondIntent.putExtra(EXTRA_QUESTION_CONTENT, content);
                setResult(RESULT_OK, respondIntent);
                finish();
            }
        });
    }

    private void prepareAnswerRadioGroup(int id, String content) {
        RadioButton rbn = new RadioButton(getApplicationContext());
        rbn.setId(id);
        rbn.setText(content);
        rbn.setHeight(140);
        rbn.setTextAppearance(getApplicationContext(), R.style.DetailsLabel);
        rbn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                questionEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                answerEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);

                answersRadioGroup.removeViewAt(v.getId());
                answers.remove(v.getId());

                displaySnackbar(getResources().getString(R.string.answer_removed));
                return true;
            }
        });

        answersRadioGroup.addView(rbn);
    }

    private void displaySnackbar(String message) {
        Snackbar.make(findViewById(R.id.edit_question_view), message,
                Snackbar.LENGTH_LONG).show();
    }
}