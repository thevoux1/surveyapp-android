package com.example.surveyapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.example.surveyapp.R;
import com.example.surveyapp.api.ApiClient;
import com.example.surveyapp.api.ApiInterface;
import com.example.surveyapp.models.User;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInActivity extends AppCompatActivity {
    private EditText loginEditText;
    private EditText passwordEditText;
    private Button logInButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        loginEditText = findViewById(R.id.login_edit_text);
        passwordEditText = findViewById(R.id.password_edit_text);
        logInButton = findViewById(R.id.log_in_button);

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ukrycie klawiatury
                loginEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                passwordEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);

                if(loginEditText.getText().toString().trim().equals("")) {
                    displaySnackbar(getResources().getString(R.string.empty_login));
                    return;
                }

                if(passwordEditText.getText().toString().trim().equals("")) {
                    displaySnackbar(getResources().getString(R.string.empty_password));
                    return;
                }

                getUsers();
            }
        });
    }

    private void displaySnackbar(String message) {
        Snackbar.make(findViewById(R.id.log_in_view), message,
                Snackbar.LENGTH_LONG).show();
    }

    private void getUsers() {
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<List<User>> call = apiInterface.getUsers();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                checkLogInData(response.body());
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                displaySnackbar(getResources().getString(R.string.something_went_wrong));
            }
        });
    }

    private void checkLogInData(List<User> users) {
        String login = loginEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        User foundUser = null;
        for(User user : users) {
            if(user.getLogin().equals(login))
            {
                foundUser = user;
                break;
            }
        }

        if(foundUser == null || !foundUser.getPassword().equals(password)) {
            displaySnackbar(getResources().getString(R.string.invalid_data));
            return;
        }

        SurveysActivity.LoggedUser = foundUser;
        setResult(RESULT_OK);
        finish();
    }
}