package com.example.surveyapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.surveyapp.R;
import com.example.surveyapp.api.ApiClient;
import com.example.surveyapp.api.ApiInterface;
import com.example.surveyapp.models.Answer;
import com.example.surveyapp.models.Question;
import com.example.surveyapp.models.Survey;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisplayQuestionActivity extends AppCompatActivity {
    public static final String EXTRA_SURVEY_ID = "SurveyID";
    private List<Question> questions;
    private List<Answer> currentAnswers;
    private List<Answer> selectedAnswers = new ArrayList<>();
    private int questionIndex = 0;

    private TextView questionContentTextView;
    private RadioGroup answersRadioGroup;
    private Button nextQuestionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_question);
        questionContentTextView = findViewById(R.id.question_content);
        answersRadioGroup = findViewById(R.id.answers_radiogroup);
        nextQuestionButton = findViewById(R.id.next_question_button);

        nextQuestionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = answersRadioGroup.getCheckedRadioButtonId();

                if(selectedId == -1)
                {
                    Snackbar.make(findViewById(R.id.question_realtive_layout), R.string.no_answer,
                            Snackbar.LENGTH_LONG).show();
                    return;
                }

                selectedAnswers.add(currentAnswers.get(selectedId));

                if(questionIndex >= questions.size()) {
                    finish();
                    sendAnswers();
                    return;
                }

                displayQuestion(questionIndex++);
            }
        });

        getQuestions();
    }

    private void getQuestions() {
        int surveyId = getIntent().getIntExtra(EXTRA_SURVEY_ID, -1);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<List<Question>> call = apiInterface.getQuestions(surveyId);
        call.enqueue(new Callback<List<Question>>() {
            @Override
            public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                questions = response.body();
                displayQuestion(0);
                questionIndex++;
            }

            @Override
            public void onFailure(Call<List<Question>> call, Throwable t) {
                Snackbar.make(findViewById(R.id.main_view), R.string.something_went_wrong,
                        Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void displayQuestion(int index) {
        Question currentQuestion = questions.get(index);
        questionContentTextView.setText((index + 1) + ") " + currentQuestion.getContent());
        getAnswers(currentQuestion.getId());
    }

    private void getAnswers(int questionIndex) {
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<List<Answer>> call = apiInterface.getAnswers(questionIndex);
        call.enqueue(new Callback<List<Answer>>() {
            @Override
            public void onResponse(Call<List<Answer>> call, Response<List<Answer>> response) {
                currentAnswers = response.body();
                setupAnswerListView();
            }

            @Override
            public void onFailure(Call<List<Answer>> call, Throwable t) {
                Snackbar.make(findViewById(R.id.main_view), R.string.something_went_wrong,
                        Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void setupAnswerListView() {
        answersRadioGroup.removeAllViews();;

        for (int i = 0; i < currentAnswers.size() ; i++) {
            RadioButton rbn = new RadioButton(this);
            rbn.setId(i);
            rbn.setText(currentAnswers.get(i).getContent());
            rbn.setHeight(140);
            rbn.setTextAppearance(getApplicationContext(), R.style.DetailsLabel);
            answersRadioGroup.addView(rbn);
        }
    }

    private void sendAnswers()
    {
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        for(Answer answer : selectedAnswers) {
            Call<Answer> call = apiInterface.updateAnswerSelections(answer.getId(), answer.getSelections() + 1);
            call.enqueue(new Callback<Answer>() {
                @Override
                public void onResponse(Call<Answer> call, Response<Answer> response) {

                }

                @Override
                public void onFailure(Call<Answer> call, Throwable t) {
                    Snackbar.make(findViewById(R.id.main_view), R.string.something_went_wrong,
                            Snackbar.LENGTH_LONG).show();
                }
            });
        }
    }
}