package com.example.surveyapp.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.surveyapp.R;
import com.example.surveyapp.api.ApiClient;
import com.example.surveyapp.api.ApiInterface;
import com.example.surveyapp.models.Survey;
import com.example.surveyapp.models.User;
import com.example.surveyapp.room.UserViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.Thread.sleep;

public class SurveysActivity extends AppCompatActivity {
    public static final int LOG_IN_ACTIVITY_REQUEST_CODE = 1;
    public static final int REGISTER_ACTIVITY_REQUEST_CODE = 2;
    public static final int CREATE_SURVEY_ACTIVITY_REQUEST_CODE = 3;
    public static final int EDIT_SURVEY_ACTIVITY_REQUEST_CODE = 4;

    public static final int LOGGED_OUT_RESULT = 2;

    public static final String EXTRA_USER_ID = "UserId";
    public static final String EXTRA_SURVEYS_PURPOSE = "SurveysPurpose";

    public static User LoggedUser = null;
    private static ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

    private FloatingActionButton addSurveyButton;
    private int surveysPurpose;
    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surveys);
        addSurveyButton = findViewById(R.id.add_survey_button);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        if(getIntent().hasExtra(EXTRA_SURVEYS_PURPOSE)) {
            if(getIntent().getIntExtra(EXTRA_SURVEYS_PURPOSE, -1) == EDIT_SURVEY_ACTIVITY_REQUEST_CODE) {
                surveysPurpose = EDIT_SURVEY_ACTIVITY_REQUEST_CODE;
                setTitle(R.string.my_surveys);
            }
        } else {
            userViewModel.findAll().observe(this, new Observer<List<User>>() {
                @Override
                public void onChanged(List<User> users) {
                    if(users != null && users.size() > 0) {
                        LoggedUser = users.get(0);
                        invalidateOptionsMenu();
                        updateSubtitle();
                    }
                }
            });
        }

        updateSubtitle();

        addSurveyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(LoggedUser == null) {
                    displaySnackbar(getString(R.string.log_in_required));
                    return;
                }

                Intent intent = new Intent(SurveysActivity.this, EditSurveyActivity.class);
                intent.putExtra(EXTRA_USER_ID, LoggedUser.getId());
                startActivityForResult(intent, CREATE_SURVEY_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(surveysPurpose == EDIT_SURVEY_ACTIVITY_REQUEST_CODE)
            getMySurveys();
        else
            getPublicSurveys();
    }

    private void getPublicSurveys() {
        Call<List<Survey>> call = apiInterface.getAllSurveys();
        call.enqueue(new Callback<List<Survey>>() {
            @Override
            public void onResponse(Call<List<Survey>> call, Response<List<Survey>> response) {
                setupSurveyListView(response.body());
            }

            @Override
            public void onFailure(Call<List<Survey>> call, Throwable t) {
                displaySnackbar(getString(R.string.something_went_wrong));
            }
        });
    }

    private void getMySurveys() {
        Call<List<Survey>> call = apiInterface.getMySurveys(LoggedUser.getId());
        call.enqueue(new Callback<List<Survey>>() {
            @Override
            public void onResponse(Call<List<Survey>> call, Response<List<Survey>> response) {
                setupSurveyListView(response.body());
            }

            @Override
            public void onFailure(Call<List<Survey>> call, Throwable t) {
                displaySnackbar(getString(R.string.something_went_wrong));
            }
        });
    }

    private void setupSurveyListView(List<Survey> surveys) {
        RecyclerView recyclerView = findViewById(R.id.surveys_recyclerview);
        final SurveyAdapter adapter = new SurveyAdapter();
        adapter.setSurveys(surveys);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private class SurveyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Survey survey;
        private TextView surveyNameTextView;
        private ImageView surveyIconImageView;

        public SurveyHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.survey_list_item, parent, false));

            surveyNameTextView = itemView.findViewById(R.id.survey_name);
            surveyIconImageView = itemView.findViewById(R.id.survey_icon);
            itemView.setOnClickListener(this);
        }

        public void bind(Survey survey) {
            this.survey = survey;
            surveyNameTextView.setText(survey.getName());
            surveyIconImageView.setImageResource(R.drawable.ic_baseline_article);
        }

        @Override
        public void onClick(View v) {
            if(surveysPurpose == EDIT_SURVEY_ACTIVITY_REQUEST_CODE) {
                Intent editIntent = new Intent(SurveysActivity.this, EditSurveyActivity.class);
                editIntent.putExtra(EditSurveyActivity.EXTRA_SURVEY_OBJECT, survey);
                startActivityForResult(editIntent, EDIT_SURVEY_ACTIVITY_REQUEST_CODE);
            } else {
                Intent intent = new Intent(SurveysActivity.this, SurveyDetailsActivity.class);
                intent.putExtra(SurveyDetailsActivity.EXTRA_SURVEY_OBJECT, survey);
                startActivityForResult(intent, EDIT_SURVEY_ACTIVITY_REQUEST_CODE);
            }
        }
    }

    private class SurveyAdapter extends RecyclerView.Adapter<SurveyHolder> {
        private List<Survey> surveys;

        @NonNull
        @Override
        public SurveyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new SurveyHolder(getLayoutInflater(), parent);
        }

        @Override
        public void onBindViewHolder(@NonNull SurveyHolder holder, int position) {
            if(surveys != null) {
                Survey survey = surveys.get(position);
                holder.bind(survey);
            }
        }

        @Override
        public int getItemCount() {
            if(surveys != null)
                return surveys.size();

            return 0;
        }

        void setSurveys(List<Survey> surveys) {
            this.surveys = surveys;
            notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        if(LoggedUser == null) {
            menu.findItem(R.id.log_in).setVisible(true);
            menu.findItem(R.id.register).setVisible(true);
            menu.findItem(R.id.log_out).setVisible(false);
            menu.findItem(R.id.my_surveys).setVisible(false);
        } else {
            menu.findItem(R.id.log_in).setVisible(false);
            menu.findItem(R.id.register).setVisible(false);
            menu.findItem(R.id.log_out).setVisible(true);
            menu.findItem(R.id.my_surveys).setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.register:
                Intent registerIntent = new Intent(SurveysActivity.this, RegisterActivity.class);
                startActivityForResult(registerIntent, REGISTER_ACTIVITY_REQUEST_CODE);
                return true;
            case R.id.log_in:
                Intent loginIntent = new Intent(SurveysActivity.this, LogInActivity.class);
                startActivityForResult(loginIntent, LOG_IN_ACTIVITY_REQUEST_CODE);
                return true;
            case R.id.refresh:
                if(surveysPurpose == EDIT_SURVEY_ACTIVITY_REQUEST_CODE) {
                    getMySurveys();
                } else {
                    getPublicSurveys();
                }
                return true;
            case R.id.log_out:
                userViewModel.delete(LoggedUser);
                LoggedUser = null;
                invalidateOptionsMenu();
                updateSubtitle();
                displaySnackbar(getString(R.string.logged_out));
                if(surveysPurpose == EDIT_SURVEY_ACTIVITY_REQUEST_CODE) {
                    setResult(LOGGED_OUT_RESULT);
                    finish();
                }
                return true;
            case R.id.my_surveys:
                if(surveysPurpose == EDIT_SURVEY_ACTIVITY_REQUEST_CODE) {
                    getMySurveys();
                } else {
                    Intent mySurveysIntent = new Intent(SurveysActivity.this, SurveysActivity.class);
                    mySurveysIntent.putExtra(EXTRA_SURVEYS_PURPOSE, EDIT_SURVEY_ACTIVITY_REQUEST_CODE);
                    startActivityForResult(mySurveysIntent, EDIT_SURVEY_ACTIVITY_REQUEST_CODE);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == LOG_IN_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            invalidateOptionsMenu();
            updateSubtitle();
            userViewModel.insert(LoggedUser);
            displaySnackbar(getString(R.string.logged_in));
        } else if(requestCode == REGISTER_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            displaySnackbar(getString(R.string.registered));
        } else if(requestCode == CREATE_SURVEY_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            if(surveysPurpose == EDIT_SURVEY_ACTIVITY_REQUEST_CODE) {
                getMySurveys();
            } else {
                getPublicSurveys();
            }
            displaySnackbar(getString(R.string.survey_created));
        } else if(requestCode == EDIT_SURVEY_ACTIVITY_REQUEST_CODE && resultCode == LOGGED_OUT_RESULT) {
            invalidateOptionsMenu();
            updateSubtitle();
        } else if(requestCode == EDIT_SURVEY_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            getMySurveys();
        }
    }

    private void displaySnackbar(String message) {
        Snackbar.make(findViewById(R.id.main_layout), message,
                Snackbar.LENGTH_LONG).show();
    }


    public void updateSubtitle() {
        String subtitle;

        if(LoggedUser == null)
            subtitle = null;
        else
            subtitle = getString(R.string.welcome_user, LoggedUser.getLogin());

        getSupportActionBar().setSubtitle(subtitle);
    }
}