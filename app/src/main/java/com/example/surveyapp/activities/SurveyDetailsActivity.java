package com.example.surveyapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.surveyapp.R;
import com.example.surveyapp.models.Survey;

public class SurveyDetailsActivity extends AppCompatActivity {
    public static final String EXTRA_SURVEY_OBJECT = "SurveyObject";
    private TextView nameTextView;
    private TextView startDateTextView;
    private TextView endDateTextView;
    private TextView descriptionTextView;
    private Button startSurveyButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_details);

        nameTextView = findViewById(R.id.survey_name_label);
        startDateTextView = findViewById(R.id.survey_start_date_label);
        endDateTextView = findViewById(R.id.survey_end_date_label);
        descriptionTextView = findViewById(R.id.survey_description_label);
        startSurveyButton = findViewById(R.id.start_survey_button);

        Survey survey = (Survey) getIntent().getSerializableExtra(EXTRA_SURVEY_OBJECT);
        nameTextView.setText(survey.getName());
        startDateTextView.setText(survey.getStartDate());
        endDateTextView.setText(survey.getEndDate());
        descriptionTextView.setText(survey.getDescription());

        startSurveyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SurveyDetailsActivity.this, DisplayQuestionActivity.class);
                intent.putExtra(DisplayQuestionActivity.EXTRA_SURVEY_ID, survey.getId());
                startActivity(intent);
                finish();
            }
        });
    }
}