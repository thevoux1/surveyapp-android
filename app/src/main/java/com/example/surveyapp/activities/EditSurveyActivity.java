package com.example.surveyapp.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.example.surveyapp.R;
import com.example.surveyapp.api.ApiClient;
import com.example.surveyapp.api.ApiInterface;
import com.example.surveyapp.models.Answer;
import com.example.surveyapp.models.Question;
import com.example.surveyapp.models.Survey;
import com.google.android.material.snackbar.Snackbar;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditSurveyActivity extends AppCompatActivity {
    public static final int NEW_QUESTION_REQUEST_CODE = 1;
    public static final int EDIT_QUESTION_REQUEST_CODE = 2;
    public static final String EXTRA_SURVEY_OBJECT = "SurveyObject";

    private int surveyId;
    private int userId;
    private Question editQuestion;
    private List<Question> questions;
    private List<Answer> answers;

    private EditText nameEditText;
    private TextView startDateTextView;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private TextView endDateTextView;
    private EditText descriptionEditText;
    private Button addQuestionButton;
    private Button createSurveyButton;
    private TextView selectedDate;

    private ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_survey);
        userId = getIntent().getIntExtra(SurveysActivity.EXTRA_USER_ID, 0);
        questions = new ArrayList<>();
        answers = new ArrayList<>();

        nameEditText = findViewById(R.id.survey_name_edit_text);
        startDateTextView = findViewById(R.id.survey_start_date);
        endDateTextView = findViewById(R.id.survey_end_date);
        descriptionEditText = findViewById(R.id.survey_details_edit_text);
        addQuestionButton = findViewById(R.id.add_question_button);
        createSurveyButton = findViewById(R.id.create_survey_button);

        if(getIntent().hasExtra(EXTRA_SURVEY_OBJECT)) {
            setTitle(R.string.edit_survey);
            createSurveyButton.setVisibility(View.INVISIBLE);
            addQuestionButton.setText(R.string.save_changes);
            Survey survey = (Survey) getIntent().getSerializableExtra(EXTRA_SURVEY_OBJECT);
            surveyId = survey.getId();
            nameEditText.setText(survey.getName());
            startDateTextView.setText(survey.getStartDate().substring(0, 10));
            endDateTextView.setText(survey.getEndDate().substring(0, 10));
            descriptionEditText.setText(survey.getDescription());
            setQuestions(surveyId);
        }

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIntent().hasExtra(EXTRA_SURVEY_OBJECT)) {
                    displaySnackbar(getResources().getString(R.string.existing_date));
                } else {
                    selectedDate = startDateTextView;
                    getDate();
                }
            }
        });

        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedDate = endDateTextView;
                getDate();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month++;
                String dayString = String.valueOf(dayOfMonth);
                String monthString = String.valueOf(month);

                if(dayOfMonth < 10)
                    dayString = "0" + dayString;
                if(month < 10)
                    monthString = "0" + monthString;

                String date = year + "-" + monthString + "-" + dayString;
                selectedDate.setText(date);
            }
        };

        createSurveyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //ukrycie klawiatury
                    nameEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                    descriptionEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                    checkData();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        addQuestionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
                descriptionEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);

                if(!getIntent().hasExtra(EXTRA_SURVEY_OBJECT)) {
                    Intent loginIntent = new Intent(EditSurveyActivity.this, EditQuestionActivity.class);
                    startActivityForResult(loginIntent, NEW_QUESTION_REQUEST_CODE);
                } else {
                    try {
                        checkData();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void setQuestions(int surveyId) {
        Call<List<Question>> call = apiInterface.getQuestions(surveyId);
        call.enqueue(new Callback<List<Question>>() {
            @Override
            public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                questions = response.body();
                setRecyclerView();
            }

            @Override
            public void onFailure(Call<List<Question>> call, Throwable t) {
                displaySnackbar(getResources().getString(R.string.something_went_wrong));
            }
        });
    }

    private void checkData() throws ParseException {
        String name = nameEditText.getText().toString();
        String startDate = startDateTextView.getText().toString();
        String endDate = endDateTextView.getText().toString();
        String description = descriptionEditText.getText().toString();

        if(name.trim().equals(""))
        {
            displaySnackbar(getResources().getString(R.string.empty_name));
            return;
        }

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date();
        currentDate.setTime(currentDate.getTime() - currentDate.getTime() % (24 * 60 * 60 * 1000));

        if(startDate.equals(getResources().getString(R.string.select_date)))
        {
            displaySnackbar(getResources().getString(R.string.no_start_date));
            return;
        }

        Date startDateDate = format.parse(startDate);

        if(!getIntent().hasExtra(EXTRA_SURVEY_OBJECT)) {
            if(startDateDate.before(currentDate))
            {
                displaySnackbar(getResources().getString(R.string.past_start_date));
                return;
            }

            if(endDate.equals(getResources().getString(R.string.select_date)))
            {
                displaySnackbar(getResources().getString(R.string.no_end_date));
                return;
            }
        }

        Date endDateDate = format.parse(endDate);

        if(endDateDate.before(startDateDate) || endDateDate.equals(startDateDate))
        {
            displaySnackbar(getResources().getString(R.string.older_end_date));
            return;
        }

        if(description.trim().equals(""))
        {
            displaySnackbar(getResources().getString(R.string.empty_description));
            return;
        }

        if(questions.isEmpty())
        {
            displaySnackbar(getResources().getString(R.string.no_question));
            return;
        }

        if(!getIntent().hasExtra(EXTRA_SURVEY_OBJECT)) {
            insertSurvey(name, description, startDate, endDate);
        } else {
            updateSurvey(name, description, startDate, endDate);
        }

        setResult(RESULT_OK);
        finish();
    }

    private void insertSurvey(String name, String description, String startDate, String endDate) {
        Call<List<Survey>> surveyCall = apiInterface.insertSurvey(userId, name, description, startDate, endDate);
        surveyCall.enqueue(new Callback<List<Survey>>() {
            @Override
            public void onResponse(Call<List<Survey>> call, Response<List<Survey>> response) {
                List<Survey> surveys = response.body();
                surveyId = surveys.get(0).getId();
                insertQuestions();
            }

            @Override
            public void onFailure(Call<List<Survey>> call, Throwable t) {
                displaySnackbar(getResources().getString(R.string.something_went_wrong));
            }
        });
    }

    private void updateSurvey(String name, String description, String startDate, String endDate) {
        Call<List<Survey>> surveyCall = apiInterface.updateSurvey(surveyId, name, description, startDate, endDate);
        surveyCall.enqueue(new Callback<List<Survey>>() {
            @Override
            public void onResponse(Call<List<Survey>> call, Response<List<Survey>> response) {

            }

            @Override
            public void onFailure(Call<List<Survey>> call, Throwable t) {
                displaySnackbar(getResources().getString(R.string.something_went_wrong));
            }
        });

    }

    public void insertQuestions() {
        for(Question question : questions) {
            Call<List<Question>> questionCall = apiInterface.insertQuestion(surveyId, question.getContent());
            questionCall.enqueue(new Callback<List<Question>>() {
                @Override
                public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                    List<Question> newQuestions = response.body();
                    int newQuestionId = newQuestions.get(0).getId();
                    insertAnswers(question.getId(), newQuestionId);
                }

                @Override
                public void onFailure(Call<List<Question>> call, Throwable t) {
                    displaySnackbar(getResources().getString(R.string.something_went_wrong));
                }
            });
        }
    }

    private void insertAnswers(int oldQuesionId, int newQuestionId) {
        for (Answer answer : answers) {
            if (answer.getQuestionID() == oldQuesionId) {
                Call<Answer> answerCall = apiInterface.insertAnswer(newQuestionId, answer.getContent());
                answerCall.enqueue(new Callback<Answer>() {
                    @Override
                    public void onResponse(Call<Answer> call, Response<Answer> response) {

                    }

                    @Override
                    public void onFailure(Call<Answer> call, Throwable t) {
                        displaySnackbar(getResources().getString(R.string.something_went_wrong));
                    }
                });
            }
        }
    }

    private void displaySnackbar(String message) {
        Snackbar.make(findViewById(R.id.edit_survey_view), message,
                Snackbar.LENGTH_LONG).show();
    }

    private void getDate() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
                EditSurveyActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                mDateSetListener,
                year, month, day);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == NEW_QUESTION_REQUEST_CODE && resultCode == RESULT_OK) {
            int questionId;
            if(questions.size() == 0)
                questionId = 0;
            else
                questionId = questions.get(questions.size() - 1).getId() + 1;

            List<Answer> newAnswers = (List<Answer>) data.getSerializableExtra(EditQuestionActivity.EXTRA_ANSWER_LIST);
            String questionContent = data.getStringExtra(EditQuestionActivity.EXTRA_QUESTION_CONTENT);

            questions.add(new Question(questionId, questionContent));
            for(Answer answer : newAnswers) {
                answer.setQuestionID(questionId);
                answers.add(answer);
            }

            setRecyclerView();
            displaySnackbar(getResources().getString(R.string.question_added));
        } else if(requestCode == EDIT_QUESTION_REQUEST_CODE && resultCode == RESULT_OK) {
            editQuestion.setContent(data.getStringExtra(EditQuestionActivity.EXTRA_QUESTION_CONTENT));
            setRecyclerView();
            answers.removeIf(x -> x.getQuestionID() == editQuestion.getId());

            List<Answer> newAnswers = (List<Answer>) data.getSerializableExtra(EditQuestionActivity.EXTRA_ANSWER_LIST);
            for(Answer answer : newAnswers)
                answers.add(answer);

            displaySnackbar(getResources().getString(R.string.question_modified));
        }
    }

    private class QuestionHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private Question question;
        private TextView questionContentTextView;

        public QuestionHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.question_list_item, parent, false));

            questionContentTextView = itemView.findViewById(R.id.list_question_content);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void bind(Question question, int position) {
            this.question = question;
            questionContentTextView.setText((position + 1) + ") " + question.getContent());
        }

        @Override
        public void onClick(View v) {
            if(!getIntent().hasExtra(EXTRA_SURVEY_OBJECT)) {
                Intent intent = new Intent(EditSurveyActivity.this, EditQuestionActivity.class);
                intent.putExtra(EditQuestionActivity.EXTRA_QUESTION_CONTENT, question.getContent());

                List<Answer> answerList = new ArrayList<>();
                for(Answer answer : answers) {
                    if(answer.getQuestionID() == question.getId())
                        answerList.add(answer);
                }

                editQuestion = question;
                intent.putExtra(EditQuestionActivity.EXTRA_ANSWER_LIST, (Serializable) answerList);
                startActivityForResult(intent, EDIT_QUESTION_REQUEST_CODE);
            } else {
                displaySnackbar(getResources().getString(R.string.edit_existing_question));
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public boolean onLongClick(View v) {
            if(!getIntent().hasExtra(EXTRA_SURVEY_OBJECT)) {
                questions.remove(question);
                setRecyclerView();
                answers.removeIf(x -> x.getQuestionID() == question.getId());
            } else {
                displaySnackbar(getResources().getString(R.string.remove_existing_question));
            }
            return true;
        }
    }

    private class QuestionAdapter extends RecyclerView.Adapter<QuestionHolder> {
        private List<Question> questions;

        @NonNull
        @Override
        public QuestionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new QuestionHolder(getLayoutInflater(), parent);
        }

        @Override
        public void onBindViewHolder(@NonNull QuestionHolder holder, int position) {
            if(questions != null) {
                Question question = questions.get(position);
                holder.bind(question, position);
            }
        }

        @Override
        public int getItemCount() {
            if(questions != null)
                return questions.size();

            return 0;
        }

        void setQuestions(List<Question> questions) {
            this.questions = questions;
            notifyDataSetChanged();
        }
    }

    private void setRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.questions_recyclerview);
        final QuestionAdapter adapter = new QuestionAdapter();
        adapter.setQuestions(questions);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }
}