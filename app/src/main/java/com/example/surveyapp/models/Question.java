package com.example.surveyapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Question {
    @Expose
    @SerializedName("question_id")
    private int id;

    @Expose
    @SerializedName("survey_id")
    private int surveyId;

    @Expose
    @SerializedName("content")
    private String content;

    @Expose
    @SerializedName("type")
    private int type;

    public Question() { }

    public Question(int id, String content) {
        this.id = id;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(int surveyId) {
        this.surveyId = surveyId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
