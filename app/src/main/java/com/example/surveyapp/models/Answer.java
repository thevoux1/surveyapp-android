package com.example.surveyapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Answer implements Serializable {
    @Expose
    @SerializedName("answer_id")
    private int id;

    @Expose
    @SerializedName("question_id")
    private int questionID;

    @Expose
    @SerializedName("selections")
    private int selections;

    @Expose
    @SerializedName("content")
    private String content;

    public Answer() { }

    public Answer(String content) {
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public int getSelections() {
        return selections;
    }

    public void setSelections(int selections) {
        this.selections = selections;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
